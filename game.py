#!/usr/bin/python

from models import *

def promptForName():
		validName = False
		while not validName:
			name = raw_input()
			if len(name) > 0:
				validName = True
		return name

def main():
	
	newGame = True

	while newGame:

		clearScreen()		

		newGame = False
		
		print 'Welcome to Child Rummy!'
		print 'Please enter your name:'

		name = promptForName()
		user = Player(name)
		user.isCPUPlayer = False
		
		players = [user]
		for name in ['Charlie','Mac','Dennis']:
			players.append(Player(name))

		game = Game(players)

		game.start()

		print 'Press enter to start your turn.'
		raw_input()

		while not game.gameOver():
			game.play()
			
		print 'play again? (Y/N)'

		newGame = raw_input().lower()
		if newGame is 'y':
			newGame = True
		else:
			print 'game over'

	exit('thanks for playing!!')


if __name__ == '__main__':
	main()