from models import *

#sorting methods, used for finding winning hands

def checkForTriplet(straight,hand):

	#remove the cards in the straight from the hand
	#then, see if the remaining cards have the same value

	copy = list(hand)

	i = 0
	while i < len(hand):
		
		card = hand[i]

		for c in straight:
			if c.suit == card.suit and c.value == card.value:
				copy.remove(card)

		i = i + 1

	lastTriplet = copy[0]
	for card in copy[1:]:
		if card.value == lastTriplet.value:
			lastTriplet = card
		else:
			return False
	return True

def checkForStraight(cards):

	if len(cards) != 4: 
		return False

	lastCard = cards[0]
	for card in cards[1:]:
		if card.value == lastCard.value + 1:
			lastCard = card
		else:
			return False
	return True


def splitBySuit(hand):

	for card in hand:
		print card.description()
	
	suitsHash = {}

	for card in hand:

		if card.suit not in suitsHash:
			suitsHash[card.suit] = []

		suitsHash[card.suit].append(card)
			
	return suitsHash
		
def mergeSortByValue(cards):

	if len(cards) > 1:
		mid = len(cards)//2
		left = mergeSortByValue(cards[:mid])
		right = mergeSortByValue(cards[mid:])
		cards = mergeValue(left,right)
	return cards

def mergeValue(left,right):

	cards = left + right

	i=0
	l=0
	r=0
	while l<len(left) and r<len(right):
		if left[l].value < right[r].value:
			cards[i] = left[l]
			l=l+1
		else:
			cards[i] = right[r]
			r=r+1
		i=i+1

	while l<len(left):
		cards[i] = left[l]
		l=l+1
		i=i+1

	while r<len(right):
		cards[i] = right[r]
		r=r+1
		i=i+1

	return cards