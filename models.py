from sorts import *
import random
import os
import sys

def suits():
	if not hasattr(suits,'val'):
		suits.val = {
			0 : u'\u2665', #heart
			1 : u'\u2666', #diamond
			2 : u'\u2660', #spade
			3 : u'\u2663' #club
			}
	
	return suits.val

def values():
	if not hasattr(values,'val'):
		
		valuesDict = {}
		for i in range(0,13):
			valuesDict[i] = i+2

		values.val = valuesDict
	
	return values.val

def clearScreen():
	os.system('cls' if os.name=='nt' else 'clear')

class Deck:

	def __init__(self):

		self.discardPile = []
		self.stack = []

		cardCount = len(values())*len(suits())

		i = 0
		while i < cardCount:
			
			value = values()[i % len(values())]
			suitIdx = i % len(suits())
			suit = suits()[suitIdx]
			card = Card(value,suit,suitIdx)
			self.stack.append(card)
			i = i + 1

	def dealFromStack(self):

		card = self.stack.pop()

		if len(self.stack) is 0:
			self.stack = self.discardPile
			self.discardPile = []
			self.shuffle()

		return card

	def dealFromDiscard(self):
		if len(self.discardPile):
			return self.discardPile.pop()
		else:
			return None	
		

	def shuffle(self):
		random.shuffle(self.stack)

	def peekAtDiscard(self):
		if len(self.discardPile) is 0:
			return None
		else:
			return self.discardPile[-1]

	def peekAtStack(self):
		if len(self.stack) is 0:
			return None
		else:
			return self.stack[-1]

	def discard(self, card):
		self.discardPile.append(card)

	def description(self):
		desc = '\n'
		for card in self.stack[::-1]:
			desc = desc + card.description() + '\n'

		return desc


class Card:
	def __init__(self,value,suit,suitComparator):
		self.value = value
		self.suit = suit
		self.suitComparator = suitComparator

	def description(self):

		#determine the 'display name' of our current value
		v = self.value
		if v is 11:
			v = 'J'
		elif v is 12:
			v = 'Q'
		elif v is 13:
			v = 'K'
		elif v is 14:
			v = 'A'
		else:
			v = str(v)

		return v + self.suit

class Player:
	def __init__(self,name):
		self.hand = []
		self.name = name
		self.isCPUPlayer = True

	def addToHand(self,card):
		self.hand.append(card)

	def discard(self,cardIndex):
		return self.hand.pop(cardIndex)

	def makeMove(self):
		toss = random.randint(0, 1)
		if toss is 0:
			return 's'
		else:
			return 'd'

	def description(self):

		desc = self.name + ' - '

		for card in self.hand:
			desc = desc + card.description() + '  '

		return desc

	def safeDescription(self):
		string = self.name + ' - '
		for card in self.hand:
			string += '|'
		return string

class Game:
	def __init__(self, players):
		self.players = players
		self.deck = Deck()
		self.cheating = False
		self.activePlayerIndex = 0
		self.turnCount = 1
		self.winner = None

	def activePlayer(self):
		return self.players[self.activePlayerIndex]

	def start(self):
		user = self.players[0]
		if user.name == 'nick':
			self.cheating = True
		elif user.name == 'otto':
			self.cheating = True
			user.isCPUPlayer = True

		self.testDeck(self.deck)

		print 'Starting game with players:'
		for player in self.players:
			print player.name

		self.deck.shuffle()

		self.deck.discard(self.deck.dealFromStack())

		for i in range(0,7):
			for player in self.players:
				player.addToHand(self.deck.dealFromStack())

	def testDeck(self,deck):

		print 'testing deck'

		assert len(deck.stack) is 52
		assert len(deck.discardPile) is 0

		constSuits = [u'\u2665',u'\u2666',u'\u2660',u'\u2663']
		constValues = [2,3,4,5,6,7,8,9,10,11,12,13,14]

		i = 0

		while i < 52:

			foundMatch = False
			
			value = constValues[i % len(constValues)]
			suit = constSuits[i % len(constSuits)]
			
			for card in deck.stack:
				foundMatch = None
				if card.suit == suit and card.value == value:
					foundMatch = card
					break

			if not foundMatch:
				print 'couldn\'t find card matching ' + str(value) + suit
				assert foundMatch
			else: 
				sys.stdout.write(card.description()+'  ')

			i = i + 1

		for suit in constSuits:
			for value in constValues:

				foundMatch = False
				for card in deck.stack:
					if card.suit == suit and card.value == value:
						foundMatch = True
						break
				
		print ''
		print 'deck passed validation'

	def gameOver(self):

		if self.turnCount >= 250:
			return True
		if self.winner:
			return True

		return False

	def endTurn(self):

		didWin = self.checkForWin(self.activePlayer().hand)

		if didWin:
			self.winner = self.activePlayer()
			print self.activePlayer().name + ' is the Winner!!!'
			return

		self.turnCount = self.turnCount + 1
		self.activePlayerIndex = self.activePlayerIndex + 1
		if self.activePlayerIndex >= len(self.players):
			self.activePlayerIndex = 0
		self.printBoard()

	def checkForWin(self,hand):
		
		print 'cfw'
		#logic here is basically:
		#split the hand into collections of suited cards
		#see if we have 4 or more cards of any one suit
		#find all of the straights in that suit
		#for each straight, see if that leaves us with a triplet
		
		# sortedHand = [
		# 			Card(values()[0],suits()[0],0),
		# 			Card(values()[1],suits()[0],0),
		# 			Card(values()[2],suits()[0],0),
		# 			Card(values()[3],suits()[0],0),
		# 			Card(values()[6],suits()[1],1),
		# 			Card(values()[6],suits()[2],2),
		# 			Card(values()[6],suits()[0],0)
		# 			]

		#hash of arrays
		currentSuits = splitBySuit(hand)

		for key in currentSuits:
			suit = mergeSortByValue(currentSuits[key])

			if len(suit) >= 4:
				print suit
				idx = 4
				while idx < len(suit):

					aSlice = suit[idx-4:idx]
					foundStraight = checkForStraight(aSlice)

					if foundStraight:
						foundTriplet = checkForTriplet(aSlice,hand)
						if foundTriplet:
							return True
					idx = idx + 1

		return False
		
	def printBoard(self):

		clearScreen()

		print 'turn #' + str(self.turnCount) + '\n'

		print 'it is ' + self.activePlayer().name + '\'s turn\n'

		stackDesc = ''
		for i in range(0,len(self.deck.stack)):
			stackDesc += '|'

		if self.cheating:
			top = self.deck.peekAtStack()
			if top:
				stackDesc += top.description()
		else:
			stackDesc += '??'
		
		print 'Stack: ' + stackDesc

		stackDesc = 'Discard Pile:'
		for i in range(0,len(self.deck.discardPile)):
			stackDesc += '|'

		top = self.deck.peekAtDiscard()
		if top:
			stackDesc += top.description()

		print stackDesc
		print ''

		if self.cheating:
			print self.players[0].description()

		for p in self.players[1:]:
			if self.cheating:
				print p.description()
			else:
				print p.safeDescription()


	def processMove(self,move):

		move = move.lower()

		if move == 'd':
			card = self.deck.dealFromDiscard()
		elif move == 's':
			card = self.deck.dealFromStack()
		else:
			return False

		p = self.activePlayer()
		p.addToHand(card)

		self.printBoard()

		return True

	def processDiscard(self,discardIndex):

		didDiscard = False
		errorMsg = 'invalid discard index'

		try: 
			discardIndex = int(discardIndex)

			if discardIndex in range(0,len(self.activePlayer().hand)):
				card = self.activePlayer().discard(discardIndex)
				self.deck.discard(card)
				didDiscard = True

		except ValueError: #int() can throw
			self.printBoard()
			print errorMsg
			return didDiscard

		self.printBoard()

		if not didDiscard:
			print errorMsg
		
		return didDiscard

	def discardMessage(self):
		string = '    '
		for char in self.activePlayer().name:
			string += ' '
		for i in range(0,len(self.activePlayer().hand)):
			string += str(i) + '   '
		return string

	def promptWithMessage(self, message):
		if not self.activePlayer().isCPUPlayer:
			print self.activePlayer().description()
		
		print message

	def getDrawInput(self):

		if self.activePlayer().isCPUPlayer:
			nextMove = self.activePlayer().makeMove()
			message = self.activePlayer().name + ' will draw next. Press enter to continue'
			self.promptWithMessage(message)
			raw_input()
		else:
			self.promptWithMessage('\nwhere to draw from? [s]tack or [d]iscard')
			nextMove = raw_input()

		return nextMove

	def getDiscardInput(self):
		if self.activePlayer().isCPUPlayer:
			
			discardIndex = 0
			print self.activePlayer().name + ' will discard next. Press enter to continue'
			raw_input()

		else:
			message = self.discardMessage() + '\n'
			message += '\nwhich index to discard? '
			message += '[0-' + str(len(self.activePlayer().hand)-1) + ']'

			self.promptWithMessage(message)
			discardIndex = raw_input()

		return discardIndex
		
	def play(self):

		game = self

		game.printBoard()

		validMove = False
		while not validMove:

			nextMove = self.getDrawInput()
			validMove = game.processMove(nextMove)
			if not validMove:
				self.printBoard()
				print 'invalid move, please try again'	
			
		validDiscard = False
		
		while not validDiscard:

			nextDiscard = self.getDiscardInput()
			validDiscard = game.processDiscard(nextDiscard)

		game.endTurn()
